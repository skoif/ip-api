const maxmindGeoip = require('geoip-lite');
const express = require('express')
const app = express()

app.use( (req,res,next)=>{

  req.realIp = req.headers['x-real-ip'] ? req.headers['x-real-ip'] : req.socket.remoteAddress
  if( process.env.LOG_REQUESTS === 'true' ){
    console.log( '[' + req.method + '] ' + req.realIp + ' -> ' + req.originalUrl )
  }

  next()

} )

app.all( '/', (req,res)=>{

  res.json( { ip: req.realIp } )

} )

app.all( '/geoip', (req,res)=>{

  let geoip = maxmindGeoip.lookup( req.realIp )
  if( geoip ){
    if( geoip.ll ){
      geoip.latitude = geoip.ll[0]
      geoip.longtitude = geoip.ll[1]
      geoip.lat = geoip.latitude
      geoip.lon = geoip.longtitude
    }
    if( geoip.area ){
      geoip.areaKm = geoip.area
      geoip.areaKilometers = geoip.area
      geoip.areaInKilometers = geoip.area
      geoip.accurancy = geoip.area
      geoip.accurancyKm = geoip.area
      geoip.accurancyKilometers = geoip.area
    }
  }

  res.json( { ip: req.realIp, geoip } )

} )

app.all( '/geoip/:ip', (req,res)=>{

  let geoip = maxmindGeoip.lookup( req.params.ip )
  if( geoip ){
    if( geoip.ll ){
      geoip.latitude = geoip.ll[0]
      geoip.longtitude = geoip.ll[1]
      geoip.lat = geoip.latitude
      geoip.lon = geoip.longtitude
    }
    if( geoip.area ){
      geoip.areaKm = geoip.area
      geoip.areaKilometers = geoip.area
      geoip.areaInKilometers = geoip.area
      geoip.accurancy = geoip.area
      geoip.accurancyKm = geoip.area
      geoip.accurancyKilometers = geoip.area
    }
  }

  res.json( { ip: req.params.ip, geoip } )
  
} )

async function Start() {
  console.log( 'Service is starting...' )

  const listen = ()=> new Promise( (resolve, reject) => { app.listen( process.env.PORT || '3000', resolve ) })
  await listen()

  console.log( 'Service started' )
}

Start().catch( (err)=>{
  console.error( err )
  process.exit( 1 )
} )